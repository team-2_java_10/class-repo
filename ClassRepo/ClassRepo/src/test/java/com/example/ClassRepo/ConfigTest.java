package com.example.ClassRepo;

import com.example.ClassRepo.config.ClassConfig;
import com.example.ClassRepo.config.SecurityConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;



import java.util.Properties;

import static javax.swing.UIManager.get;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.springframework.security.config.Customizer.withDefaults;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class ConfigTest {


    @Test
    public void testRestTemplateBean() throws NoSuchMethodException {
        ClassConfig classConfig = new ClassConfig();
        RestTemplate restTemplate = classConfig.restTemplate();

        // Assert that restTemplate is not null
        assertNotNull(restTemplate);

        // Check if restTemplate is annotated with @LoadBalanced
        assertTrue(classConfig.getClass().getMethod("restTemplate")
                .isAnnotationPresent(LoadBalanced.class));
    }

    @Test
    public void testPropertiesBean() {
        ClassConfig classConfig = new ClassConfig();

        Properties properties = classConfig.properties();

        // Assert that properties is not null
        assertNotNull(properties);
    }





}

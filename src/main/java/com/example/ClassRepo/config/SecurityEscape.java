package com.example.ClassRepo.config;

import com.example.ClassRepo.exceptions.BadRequestException;
import com.example.ClassRepo.request.ClassUpdateRequest;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


public class SecurityEscape {


    public static void checkUpdateRequest(ClassUpdateRequest request) {
        if(cleanIt(request.getClassName()).isEmpty()
                || cleanIt(request.getClassCode()).isEmpty()
                || cleanIt(request.getFsu()).isEmpty()
                || cleanIt(request.getLocation()).isEmpty()
                || cleanIt(request.getEndDate()).isEmpty()
                || cleanIt(request.getStartDate()).isEmpty()
                || cleanIt(request.getClassTime()).isEmpty()
                || cleanIt(request.getModifyBy()).isEmpty()
                || cleanIt(request.getSchedule().toString()).isEmpty()
                || cleanIt(request.getTrainingForClassList().toString()).isEmpty()
                || cleanIt(request.getAdmin_id().toString()).isEmpty()
                || cleanIt(request.getClass_id().toString()).isEmpty()
        ){
            throw new BadRequestException("Unsafe request");
        }
    }


    public static String cleanIt(String request) {
        return Jsoup.clean(
                request
                , Whitelist.basic());
    }


}

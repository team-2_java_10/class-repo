FROM openjdk:11
EXPOSE 8080
ADD target/class.jar class.jar
ENTRYPOINT [ "java", "-jar" , "/class.jar" ]
